import secret as s
import json
import time
from urllib import request, parse

default_url = s.protocol + '://' + s.server + ':' + s.port
urls = {
    'start': '/server/%s/start',
    'stop': '/server/%s/stop',
    'cmd': '/server/%s/console'
}

def oauth2():
    auth_url = s.protocol + '://' + s.server + '/oauth2/token/request'
    data = parse.urlencode({'grant_type':'client_credentials','client_id':s.client_id,'client_secret':s.client_secret}).encode()
    req = request.Request(auth_url, data=data)
    resp = request.urlopen(req)
    resp_text = resp.read().decode('utf-8')
    if 'error' in resp_text:
        print('Error: ' + resp_text)
        exit
    else:
        resp_obj = json.loads(resp_text)
        token = resp_obj['access_token']
    return token

def cmd(ccmd, srv_id, token):
    url = default_url + urls['cmd'] % srv_id
    data = ccmd.encode()
    headers = {'Authorization': 'Bearer ' + token}
    req = request.Request(url, data=data, headers=headers)
    resp = request.urlopen(req)
    return resp.read().decode('utf-8')

def start(srv_id, token):
    return simple_get('start', srv_id, token)

def stop(srv_id, token):
    return simple_get('stop', srv_id, token)

def soft_stop(kick_msg, srv_id, token):
    cmd('say Stopping server in 1 minute. Reason: ' + kick_msg, srv_id, token)
    time.sleep(60)
    cmd('kickall ' + kick_msg, srv_id, token)
    cmd('stop', srv_id, token)

def soft_restart(srv_id, token):
    soft_stop('Restarting the server', srv_id, token)
    start(srv_id, token)

def simple_get(func, srv_id, token):
    url = default_url + urls[func] % srv_id
    headers = {'Authorization': 'Bearer ' + token}
    req = request.Request(url, headers=headers)
    resp = request.urlopen(req)
    return resp.read().decode('utf-8')

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("-a", "--action", dest="action",
                        help="Start SERVER_ACTION", metavar="SERVER_ACTION") 
    parser.add_argument("-c", "--command", dest="command",
                        help="Command to run if action is 'cmd'", metavar="CMD")
    parser.add_argument("-m", "--message", dest="message",
                        help="Message to print to players before stopping", metavar="MSG")

    args = parser.parse_args()

    if type(args.action) == type(None):
        parser.print_help()
    else:
        if len(args.action) > 0:
            token = oauth2()
            if token is not False:                    
                if args.action == "start":
                    start(s.server_hash, token)
                elif args.action == "stop":
                    stop(s.server_hash, token)
                elif args.action == "cmd":
                    cmd(args.command, s.server_hash, token)
                elif args.action == "softstop":
                    soft_stop(args.message, s.server_hash, token)